import React, { useEffect, useState } from 'react'
import {
    StyleSheet,
    Text,
    View,
    Image,
    ScrollView
} from 'react-native'
import {
    ProfileDetail, Button,
    Search, Input, Gap, Header
} from '../../components'
import { ILHomePage, ILNullPhoto, me, mikasa } from '../../assets';
import { colors } from '../../utils';
import { useDispatch } from 'react-redux';
import { Fire } from '../../config';

const EditProfile = ({ navigation, route }) => {
    const uid = route.params
    console.log(uid)
    const [admin, setAdmin] = useState([])
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch({ type: 'SET_LOADING', value: true })
        Fire.database()
            .ref(`admin/${uid}`)
            .once('value')
            .then(res => {
                console.log('data val listroom: ', res.val())
                if (res.val()) {
                    const oldData = res.val()
                    const data = []
                    Object.keys(oldData).map((key) => {
                        data.push({
                            id: key,
                            data: oldData[key]
                        })
                    })
                    setAdmin(data)
                }
                dispatch({ type: 'SET_LOADING', value: false })
            })
            .catch(err => {
                showError(err.message)
                dispatch({ type: 'SET_LOADING', value: false })
            })
        console.log('data rooms di dlm useEffect: ', admin)
        console.log('data admin: ', uid.nama)
    }, [])

    return (
        <>
            <Header tittle="Update Profile" onPress={() => navigation.goBack()} />
            <View style={styles.container}>
                <ScrollView>
                    <View style={styles.photo}>
                        <View style={styles.avatarWrapper}>
                            <Image style={styles.avatar} source={me } />
                        </View>
                    </View>
                    <View>
                        <Input
                            label="Name"
                            value= {admin.nama}
                        />
                        <Gap height={24} />
                        <Input
                            label="Email"
                            value={admin.email}
                            editable={false}
                            selectTextOnFocus={false}
                        />
                    </View>
                    <Gap height={64} />
                </ScrollView>
            </View>
        </>
    )
}
export default EditProfile;

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 40,
        flex: 1,
        backgroundColor: colors.white,
        flex: 1
    },
    photo: {
        marginVertical: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    avatar: {
        width: 110,
        height: 110,
        borderRadius: 110 / 2
    },
    avatarWrapper: {
        width: 130,
        height: 130,
        borderWidth: 1,
        borderColor: colors.border,
        borderRadius: 130 / 2,
        alignItems: 'center',
        justifyContent: 'center'
    },
})
