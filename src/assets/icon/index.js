import IconProfileActive from './ic-profile-active.svg'
import IconRemovePhoto from './ic-remove-photo.svg'
import IconHomeActive from './ic-home-active.svg'
import IconMapsActive from './ic-maps-active.svg'
import IconBackDark from './ic-back-dark.svg'
import IconAddPhoto from './ic-add-photo.svg'
import IconProfile from './ic-profile.svg'
import IconSearch from './ic-search-active.svg'
import IconTrash from './ic-trash-active.svg'
import IconHome from './ic-home.svg'
import IconMaps from './ic-maps.svg'
import IconNext from './ic-next-active.svg'
import IconRoom from './ic-room-active.svg'
import IconUser from './ic-user-active.svg'
import IconAddRoom from './ic-add-room.png'
import IconAddRoom1 from './ic_add_circle.svg'
import IconImage from './ic-image.jpg'
import IconCeklisActive from './ic-ceklis-active.svg'
import IconMapsUser from './ic-maps-user.png'
import IconRefresh from './ic-refresh.svg'
import IconEdit from './ic-edit.svg'
import IconClose from './ic-close.svg'
import IconDeveloper from './ic-developer.svg'
import IconEditProfile from './ic-edit-profile.svg'
import IconPassword from './ic-password.svg'
import IconLogout from './ic-logout.svg'

export {
    IconProfileActive,
    IconRemovePhoto,
    IconHomeActive,
    IconMapsActive,
    IconBackDark,
    IconAddPhoto,
    IconProfile,
    IconSearch,
    IconTrash,
    IconHome,
    IconMaps,
    IconNext,
    IconRoom,
    IconUser,
    IconAddRoom,
    IconAddRoom1,
    IconImage,
    IconCeklisActive,
    IconMapsUser,
    IconRefresh,
    IconEdit,
    IconClose,
    IconDeveloper,
    IconEditProfile,
    IconPassword,
    IconLogout
}