import React, { useEffect, useState } from 'react';
import { ScrollView, StyleSheet, View, PermissionsAndroid, Text, Image, TouchableOpacity } from 'react-native';
import { IconImage } from '../../assets';
import { Button, Gap, Header, Input, MenuHome } from '../../components';
import { colors, showError, useForm } from '../../utils';

import { launchImageLibrary } from 'react-native-image-picker';
import { showMessage } from 'react-native-flash-message'
import { useDispatch } from 'react-redux'


const AddRoom = ({ navigation }) => {
  const [photoForDB, setPhotoForDB] = useState('')
  const [hasPhoto, setHasPhoto] = useState(false)
  const [photo, setPhoto] = useState(IconImage)

  const getImage = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: "Cool Photo App Camera Permission",
          message:
            "Cool Photo App needs access to your camera " +
            "so you can take awesome pictures.",
          buttonNeutral: "Ask Me Later",
          buttonNegative: "Cancel",
          buttonPositive: "OK"
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the camera");
        launchImageLibrary(
          { quality: 0.2, includeBase64: true },
          (response) => {
            if (response.didCancel || response.error) {
              showError('oops, sepertinya anda tidak memilih foto nya?')
            } else {
              console.log('response : ', response.assets[0])
              const source = {uri : response.assets[0].uri}
              setPhotoForDB(`data:${response.assets[0].type};base64, ${response.assets[0].base64}`)
              setPhoto(source)
              setHasPhoto(true)
              console.log('isi photo : ', photo)
            }
          },
        )

      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }

  const [room, setRoom] = useForm({
    kategori: 'Lembaga',
    prodi: '',
    ruangan: '',
    deskripsi: '',
  })

  const [itemKategori] = useState([
    {
      id: 1,
      label: 'Lembaga',
      value: 'Lembaga'
    },
    {
      id: 2,
      label: 'Fakultas',
      value: 'Fakultas'
    },
    {
      id: 3,
      label: 'Unit',
      value: 'Unit'
    }, {
      id: 4,
      label: 'Fasilitas Umum',
      value: 'Fasilitas Umum'
    }
  ])

  const onContinue = () => {
    const dataRoom = {
      Gambar: photoForDB,
      Kategori: room.kategori,
      Prodi: room.prodi,
      Ruangan: room.ruangan,
      Deskripsi: room.deskripsi,
    }
    console.log('isi data room ', dataRoom)
    navigation.replace('AddMaps', dataRoom)
  }

  return (
    <View style={styles.page}>
      <Header onPress={() => navigation.goBack()} tittle={"Add Room"} />
      <ScrollView>
        <TouchableOpacity onPress={() => getImage()}>
          <Image source={photo} style={styles.avatar} />
        </TouchableOpacity>
        <Gap height={16} />
        <View style={styles.content}>
          <Input
            label="Kategori"
            value={room.kategori}
            select
            selectItem={itemKategori}
            onValueChange={(value) => setRoom('kategori', value)}
          />
          <Gap height={8} />

          {room.kategori === 'Fakultas' ?
            (
              <Input
                label="Prodi"
                value={room.prodi}
                onChangeText={(value) => setRoom('prodi', value)}
              />
            )
            :
            (<Input
              label="Prodi"
              value={room.prodi}
              onChangeText={(value) => setRoom('prodi', value)}
              editable={false} selectTextOnFocus={false}
              style={{ backgroundColor: '#D9DCE2' }}
            />)
          }
          <Gap height={8} />
          <Input
            label="Ruangan"
            value={room.ruangan}
            onChangeText={(value) => setRoom('ruangan', value)}
          />
          <Gap height={8} />
          <Input
            label="Deskripsi"
            value={room.deskripsi}
            onChangeText={(value) => setRoom('deskripsi', value)}
            textArea
          />
        </View>
      </ScrollView>
      <View style={styles.icon} >
        <Button type="icon-only" icon="ceklis-active" onPress={onContinue} />
      </View>
    </View>
  )
}
export default AddRoom;

const styles = StyleSheet.create({
  avatar: {
    width: '100%',
    height: 200,

  },
  node: {
    minWidth: 200,
    flex: 1,
    paddingRight: 8,
  },
  maps: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20,

  },
  geo: {
    flexDirection: 'row',
    flex: 1,
  },
  icon: {
    alignItems: 'center',
    position: 'absolute',
    bottom: 10,
    right: 10,
    zIndex: 1,
    backgroundColor: colors.secondary,
    padding: 12,
    justifyContent: 'center',
    borderRadius: 57 / 2,
    borderWidth: 3,
    borderColor: colors.primary
  },
  page: {
    backgroundColor: colors.white,
    flex: 1,
  },
  content: {
    padding: 40,
    paddingTop: 0,
    marginBottom: 40,
  }
})
