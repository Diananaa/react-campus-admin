import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import { BottomNavigator } from '../components/molecules';
import {
  AddAdmin,
  AddMaps,
  AddRoom,
  AdminDetailRoom,
  DetailMaps,
  DetailPhoto,
  DetailRoom,
  DetailUser,
  Developer,
  EditProfile,
  Home,
  ListAdmin,
  ListRoom,
  ListUser,
  Login,
  Maps,
  Profile, Splash
} from '../pages';

const Stack = createStackNavigator()
const Tab = createBottomTabNavigator();

const MainApp = () =>{
  return (
    <Tab.Navigator tabBar={props => <BottomNavigator {...props} />}>
        <Tab.Screen name="Home" component={Home} />
        <Tab.Screen name="Maps" component={Maps} />
        <Tab.Screen name="Profile" component={Profile} />
    </Tab.Navigator>
)
  }
const Router = () => {
  return(
    <Stack.Navigator initialRouteName="MainApp">
        <Stack.Screen name="Splash" component={Splash} options={{ headerShown: false }}  />
        <Stack.Screen name="Login" component={Login} options={{ headerShown: false }}  />
        <Stack.Screen name="MainApp" component={MainApp} options={{ headerShown: false }}  />
        <Stack.Screen name="AddMaps" component={AddMaps} options={{ headerShown: false }}  />
        <Stack.Screen name="AddRoom" component={AddRoom} options={{ headerShown: false }}  />
        <Stack.Screen name="AdminDetailRoom" component={AdminDetailRoom} options={{ headerShown: false }}  />
        <Stack.Screen name="DetailRoom" component={DetailRoom} options={{ headerShown: false }}  />
        <Stack.Screen name="ListRoom" component={ListRoom} options={{ headerShown: false }}  />
        <Stack.Screen name="DetailPhoto" component={DetailPhoto} options={{ headerShown: false }}  />
        <Stack.Screen name="DetailMaps" component={DetailMaps} options={{ headerShown: false }}  />
        <Stack.Screen name="ListUser" component={ListUser} options={{ headerShown: false }}  />
        <Stack.Screen name="ListAdmin" component={ListAdmin} options={{ headerShown: false }}  />
        <Stack.Screen name="AddAdmin" component={AddAdmin} options={{ headerShown: false }}  />
        <Stack.Screen name="DetailUser" component={DetailUser} options={{ headerShown: false }}  />
        <Stack.Screen name="Developer" component={Developer} options={{ headerShown: false }}  />
        <Stack.Screen name="EditProfile" component={EditProfile} options={{ headerShown: false }}  />
    </Stack.Navigator>
  )
}
export default Router;


