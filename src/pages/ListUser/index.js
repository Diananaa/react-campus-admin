import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, ScrollView, Alert } from 'react-native'
import { Button, Gap, Header, Input, MenuHome, List, ProfileDetail } from '../../components';
import { colors, showError, useForm } from '../../utils';
import { Fire } from '../../config'
import { useDispatch } from 'react-redux'

const ListUser = ({ navigation }) => {
    const [user, setUser] = useState([])
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch({ type: 'SET_LOADING', value: true })
        Fire.database()
            .ref('pengguna/')
            .once('value')
            .then(res => {
                console.log('data val listroom: ', res.val())
                if (res.val()) {
                    const oldData = res.val()
                    const data = []
                    Object.keys(oldData).map((key) => {
                        data.push({
                            id: key,
                            data: oldData[key]
                        })
                    })
                    setUser(data)
                }
                dispatch({ type: 'SET_LOADING', value: false })
            })
            .catch(err => {
                showError(err.message)
                dispatch({ type: 'SET_LOADING', value: false })
            })
        console.log('data rooms di dlm useEffect: ', user)
    }, [])

    const DeleteUser = ({item}) =>{
        Alert.alert(
            "User delete",
            `Do you want to delete this User ${item.data.name} ? You cannot undo this action`,
            [
                { text: 'NO', style: 'cancel' },
                {
                    text: 'YES', onPress: () => {
                        dispatch({ type: 'SET_LOADING', value: true })
                        console.log(item.id)
                        navigation.replace('ListUser')

                        Fire.database()
                            .ref(`pengguna/${item.id}`)
                            .remove()
                            .then(() => {
                                console.log('succes dihapus ke database')
                                dispatch({ type: 'SET_LOADING', value: false })
                            }).catch((err) => {
                                showError(err.message)
                                dispatch({ type: 'SET_LOADING', value: false })
                            })
                    }
                },
            ]
        );
    }

    return (
        <View style={styles.page}>
            <Header tittle="List User" onPress={() => navigation.goBack()} />
            <ScrollView>
                {
                    user.map((item, index) => {
                        return (
                            <List
                            key={index}
                                name={item.data.name}
                                desc={item.data.email}
                                icon="close" 
                                deletPress={()=> DeleteUser({item})}
                            />
                        )
                    })
                }
            </ScrollView>
        </View>
    )
}
export default ListUser;

const styles = StyleSheet.create({
    page: {
        backgroundColor: colors.white,
        flex: 1
    },
    icon: {
        alignItems: 'center',
        position: 'absolute',
        bottom: 10,
        right: 10,
        zIndex: 1,

    },
})
