import React, { useEffect, useState } from 'react'
import { Alert, ScrollView, StyleSheet, View } from 'react-native'
import { useDispatch } from 'react-redux'
import {
    AdminListDetailRoom, Button, Gap, Header, Search
} from '../../components'
import { Fire } from '../../config'
import { colors, showError } from '../../utils'

const ListRoom = ({ navigation }) => {
    const dispatch = useDispatch()
    const [room, setRoom] = useState([])

    useEffect(() => {
        dispatch({ type: 'SET_LOADING', value: true })
        Fire.database()
            .ref('rooms/')
            .once('value')
            .then(res => {
                console.log('data val listroom: ', res.val())
                if (res.val()) {
                    const oldData = res.val()
                    const data = []
                    Object.keys(oldData).map((key) => {
                        data.push({
                            id: key,
                            data: oldData[key]
                        })
                    })
                    setRoom(data)
                }
                dispatch({ type: 'SET_LOADING', value: false })
            })
            .catch(err => {
                showError(err.message)
                dispatch({ type: 'SET_LOADING', value: false })
            })
        console.log('data rooms di dlm useEffect: ', room)
    }, [])

    const DeleteRoom = (({ item }) => {
        Alert.alert(
            "Room delete",
            `Do you want to delete this room ${item.data.Ruangan} ? You cannot undo this action`,
            [
                { text: 'NO', style: 'cancel' },
                {
                    text: 'YES', onPress: () => {
                        dispatch({ type: 'SET_LOADING', value: true })
                        console.log(item.id)
                        navigation.replace('ListRoom')

                        Fire.database()
                            .ref(`rooms/${item.id}`)
                            .remove()
                            .then(() => {
                                console.log('succes dihapus ke database')
                                dispatch({ type: 'SET_LOADING', value: false })
                            }).catch((err) => {
                                showError(err.message)
                                dispatch({ type: 'SET_LOADING', value: false })
                            })
                    }
                },
            ]
        );
    })

    return (
        <View style={styles.container}>
            <Header tittle='List Room' onPress={() => navigation.goBack()} />
            <View style={styles.icon} >
                <Button type="icon-only" icon='add-active' onPress={() => navigation.navigate('AddRoom')} />
            </View>
            {/* <Search /> */}
            <ScrollView style={styles.page}>
                <View>
                    {room.map((item, index) => {
                        return (
                            <AdminListDetailRoom
                                key={index}
                                gambar={item.data.Gambar}
                                kategori={item.data.Kategori}
                                prodi={item.data.Prodi}
                                ruangan={item.data.Ruangan}
                                deskripsi={item.data.Deskripsi}
                                onPress={() => navigation.navigate('AdminDetailRoom', { item })}
                                onPressDelete={() => DeleteRoom({ item })}
                            />
                        )
                    })}

                    <Gap height={50} />
                </View>

            </ScrollView>

        </View >
    )
}
export default ListRoom;

const styles = StyleSheet.create({
    img: {
        width: 80,
        height: 80,
    },
    icon: {
        alignItems: 'center',
        position: 'absolute',
        bottom: 10,
        right: 10,
        zIndex: 1,

    },
    container: {
        flex: 1,
        backgroundColor: colors.white,

    },
    page: {
        marginVertical: 8,
        marginHorizontal: 24,
    }
})
