import React, { useEffect } from 'react'
import { StyleSheet, Text, View, ScrollView, Image, TouchableOpacity, Alert } from 'react-native'
import {
  Button,
  Header,
  ListDetailRoom,
  Search,
  Gap,
  Row
} from '../../components'
import { IconImage, IconTrash, IconEdit, IconMapsActive } from '../../assets'
import { colors, fonts, showError } from '../../utils'
import { Fire } from '../../config'
import { useDispatch } from 'react-redux'

const AdminDetailRoom = ({ navigation, route }) => {
  const dispatch = useDispatch()
  const dataRoom = route.params
  console.log('isi data', dataRoom)
  console.log('isi id', dataRoom.item.id)

  // useEffect(() => {
  // const dataRoom = route.params

  // const idDelete = dataRoom.item.id

  // DeleteRoom(dataRoom.item.id)
  // })

  const DeleteRoom = ((item) => {
    console.log('isi id di header: ', item)
    // alert('haii')
    Alert.alert(
      "Room delete",
      `Do you want to delete this room ? You cannot undo this action`,
      [
        { text: 'NO', style: 'cancel' },
        {
          text: 'YES', onPress: () => {
            console.log(item.id)
            navigation.replace('ListRoom')

            Fire.database()
              .ref(`rooms/${item}`)
              .remove()
              .then(() => {
                console.log('succes dihapus ke database')
                dispatch({ type: 'SET_LOADING', value: false })
              }).catch((err) => {
                showError(err.message)
                dispatch({ type: 'SET_LOADING', value: false })
              })
          }
        },
      ]
    );
  })
  return (
    <>
      <View style={styles.container}>
        <Button type="icon-only" icon="back-dark" onPress={() => navigation.goBack()} />
        <Text style={styles.text}>{dataRoom.item.data.Ruangan}</Text>
        <TouchableOpacity onPress={() => navigation.navigate('DetailMaps', dataRoom)} >
          {/* <IconEdit /> */}
          <IconMapsActive />
        </TouchableOpacity>
        <Gap width={10} />
        <TouchableOpacity onPress={() => DeleteRoom(dataRoom.item.id)}>
          <IconTrash />
        </TouchableOpacity>

        <Gap width={24} />
      </View>

      <View style={styles.content}>
        <TouchableOpacity style={styles.photo} onPress={() => navigation.navigate('DetailPhoto', dataRoom.item.data.Gambar)}>
          <Image source={{ uri: dataRoom.item.data.Gambar }} style={styles.img} />
        </TouchableOpacity>
        <ScrollView>
          <Row tb1={'KATEGORI'} tb2={dataRoom.item.data.Kategori} />
          <Row tb1={'PRODI'} tb2={dataRoom.item.data.Prodi} />
          <View style={styles.des}>
            <Text style={styles.title}>DESKRIPSI</Text>
            <Text>{dataRoom.item.data.Deskripsi}</Text>
          </View>
          <Gap height={170} />
        </ScrollView>
      </View>
    </>
  )
}
export default AdminDetailRoom;

const styles = StyleSheet.create({
  photo:{
    position: 'absolute',
    bottom: 0,
    left: 40,
    zIndex: 30,
},
  container: {
    paddingHorizontal: 16,
    paddingVertical: 15,
    backgroundColor: colors.white,
    flexDirection: 'row',
    alignItems: 'center'
  },
  text: {
    textAlign: 'center',
    flex: 1,
    fontFamily: 'Nunito-SemiBold',
    color: colors.text.primary
  },
  header: {
    // flex: 1,
    // backgroundColor: 'yellow',
    flexDirection: 'row',

  },
  img: {
    width: 100,
    height: 150,
    borderRadius: 5,
  },
  title: {
    fontFamily: fonts.primary[900],
    color: colors.text.primary,
    fontSize: 14,
  },
  des: {
    fontSize: 14,
    fontFamily: fonts.primary[500],
    color: colors.text.primary,
    marginBottom: 8,
    textAlign: 'justify',
  },
  content: {
    paddingHorizontal: 40,
    paddingTop: 0,
    flex: 1,
    backgroundColor: colors.white
  }
})
