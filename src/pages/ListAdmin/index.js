import React, { useEffect, useState } from 'react';
import { Alert, ScrollView, StyleSheet, View } from 'react-native';
import { useDispatch } from 'react-redux';
import { Button, Header, List } from '../../components';
import { Fire } from '../../config';
import { colors, showError } from '../../utils';

const ListAdmin = ({ navigation }) => {
    const [user, setUser] = useState([])
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch({ type: 'SET_LOADING', value: true })
        Fire.database()
            .ref('admin/')
            .once('value')
            .then(res => {
                console.log('data val listroom: ', res.val())
                if (res.val()) {
                    const oldData = res.val()
                    const data = []
                    Object.keys(oldData).map((key) => {
                        data.push({
                            id: key,
                            data: oldData[key]
                        })
                    })
                    setUser(data)
                }
                dispatch({ type: 'SET_LOADING', value: false })
            })
            .catch(err => {
                showError(err.message)
                dispatch({ type: 'SET_LOADING', value: false })
            })
        console.log('data rooms di dlm useEffect: ', user)
    }, [])

    const deleteAdmin = ({ item }) => {
        Alert.alert(
            "Admin delete",
            `Do you want to delete this Admin ${item.data.name} ? You cannot undo this action`,
            [
                { text: 'NO', style: 'cancel' },
                {
                    text: 'YES', onPress: () => {
                        dispatch({ type: 'SET_LOADING', value: true })
                        console.log(item.id)
                        navigation.replace('ListAdmin')

                        Fire.database()
                            .ref(`admin/${item.id}`)
                            .remove()
                            .then(() => {
                                console.log('succes dihapus ke database')
                                dispatch({ type: 'SET_LOADING', value: false })
                            }).catch((err) => {
                                showError(err.message)
                                dispatch({ type: 'SET_LOADING', value: false })
                            })
                    }
                },
            ]
        );
    }
    return (
        <View style={styles.page}>
            <Header tittle="List Admin" onPress={() => navigation.navigate('MainApp')} />
            <View style={styles.icon} >
                <Button type="icon-only" icon='add-active' onPress={() => navigation.navigate('AddRoom')} />
            </View>
            <ScrollView>
                {
                    user.map((item, index) => {
                        return (
                            <List
                                key={index}
                                name={item.data.nama}
                                desc={item.data.email}
                                icon="close"
                                deletPress={() => deleteAdmin({item})}
                            />
                        )
                    })
                }

            </ScrollView>
            <View style={styles.icon} >
                <Button type="icon-only" icon="add-active" onPress={() => navigation.navigate('AddAdmin')} />
            </View>
        </View>
    )
}
export default ListAdmin;

const styles = StyleSheet.create({
    icon: {
        alignItems: 'center',
        position: 'absolute',
        bottom: 10,
        right: 10,
        zIndex: 1,

    },
    page: {
        backgroundColor: colors.white,
        flex: 1
    },
    icon: {
        alignItems: 'center',
        position: 'absolute',
        bottom: 10,
        right: 10,
        zIndex: 1,

    },
})
