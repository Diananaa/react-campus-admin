import React from 'react'
import { StyleSheet, TouchableOpacity, View } from 'react-native'
import { IconAddRoom1, IconBackDark, IconCeklisActive } from '../../../assets'

const IconOnly = ({ onPress, icon }) => {
  const Icon = () => {
    if (icon === 'back-dark') {
      return <IconBackDark />
    }
    if (icon === 'back-light') {
      return <IconBackDark />
    }
    if (icon === 'add-active') {
      return <IconAddRoom1 />
    }
    if (icon === 'ceklis-active') {
      return <IconCeklisActive />
    }
    return <IconBackDark />
  }
  return (
    <TouchableOpacity onPress={onPress}>
      <Icon />
    </TouchableOpacity>
  )
}
export default IconOnly;

const styles = StyleSheet.create({
  icon: {
    alignItems: 'center',
    position: 'absolute',
    bottom: 10,
    right: 10,
    zIndex: 1,


  },
})
