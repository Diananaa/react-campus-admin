import React, { useEffect, useState } from 'react';
import { Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { IconSearch, ILHomePage } from '../../assets';
import {
  Gap,
  ListDetailRoom,
  MenuHome, ILRoom
} from '../../components';
import { colors, showError } from '../../utils'
import { Fire } from '../../config'
import { useDispatch } from 'react-redux'

const Home = ({ navigation }) => {

  const dispatch = useDispatch()
  const [room, setRoom] = useState([])
  
  // useEffect(() => {
  //   dispatch({ type: 'SET_LOADING', value: true })
  //   Fire.database()
  //     .ref('rooms/')
  //     .once('value')
  //     .then(res => {
  //       console.log('data val listroom: ', res.val())
  //       if (res.val()) {
  //         const oldData = res.val()
  //         const data = []
  //         Object.keys(oldData).map((key) => {
  //           data.push({
  //             id: key,
  //             data: oldData[key]
  //           })
  //         })
  //         setRoom(data)
  //       }
  //       dispatch({ type: 'SET_LOADING', value: false })
  //     })
  //     .catch(err => {
  //       showError(err.message)
  //       dispatch({ type: 'SET_LOADING', value: false })
  //     })
  //   console.log('data rooms di dlm useEffect: ', room)
  // }, [])

  return (
    <>
      <ScrollView>
        <View >
          <Image style={{ height: 200, width: '100%' }} source={ILHomePage} />
          <View style={styles.page}>

            <View style={styles.container}>
                <View style={styles.menu}>
                  <MenuHome title='ROOM' onPress={() => navigation.navigate('ListRoom')} />
                  <Gap width={8} />
                  <MenuHome title='USER' onPress={() => navigation.navigate('ListUser')}/>
                  <Gap width={8} />
                  <MenuHome title='ADMIN' onPress={() => navigation.navigate('ListAdmin')}/>
                </View>
              </View>

            {/* {room.map((item, index) => {
              return (
                <ListDetailRoom
                  keyExtractor={index}
                  // key={index}
                  gambar={item.data.Gambar}
                  kategori={item.data.Kategori}
                  prodi={item.data.Prodi}
                  ruangan={item.data.Ruangan}
                  deskripsi={item.data.Deskripsi}
                  onPress={() => navigation.navigate('DetailRoom', { item })}
                />
              )
            })} */}

          </View>
        </View>
      </ScrollView>


    </>
  )
}
export default Home;

const styles = StyleSheet.create({
  img: {
    height: 24,
    width: 230,
  },
  textSearch: {
    color: "#9FA5AF",
    flex: 1,
  },
  search: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: colors.border1.sec,
    borderRadius: 10,
    marginVertical: 8,
    marginHorizontal: 24,
    paddingHorizontal: 8,
    alignItems: 'center',
    height: 40,
    paddingHorizontal: 8,
  },
  page: {
    marginVertical: 8,
    marginHorizontal: 24,
  },
  container: {

    alignContent: 'center',
    alignItems: 'center',
  },
  menu: {
    flexDirection: 'row',
    flex: 1,

  }
})
